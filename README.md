### What is this repository for? ###

* Recruitment purposes

### How do I get running tests? ###

```gradle test -PallegroClientId=<your_app_client_id> -PallegroClientSecret=<your_app_client_secret>```

### Spec details 

* gradle 6.5.1
* java 11 
* kotlin 1.4.10
