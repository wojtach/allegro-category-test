package allegro.category

import allegro.category.RestAssuredSpec.configureRestAssuredSpecs
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.Matchers.`is`
import java.util.*

object AuthModule {
    private val clientId: String = System.getProperty("allegroClientId")
    private val clientSecret: String = System.getProperty("allegroClientSecret")

    fun authToken(): String {
        configureRestAssuredSpecs()
        val token = tokenizer()
        return Given { header("Authorization", "Basic $token") }
                .When { post("https://allegro.pl/auth/oauth/token?grant_type=client_credentials") }
                .Then { statusCode(`is`(200)) }
                .Extract { body().to<AllegroAuthResponse>() }.access_token
    }


    private fun tokenizer(): String? {
        val byteArray = "$clientId:$clientSecret".encodeToByteArray()
        val token = Base64.getEncoder().withoutPadding().encodeToString(byteArray)
        return token
    }
}



