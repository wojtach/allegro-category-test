package allegro.category

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.restassured.RestAssured
import io.restassured.builder.RequestSpecBuilder
import io.restassured.config.ObjectMapperConfig
import io.restassured.config.RestAssuredConfig
import io.restassured.response.ResponseBodyExtractionOptions

object RestAssuredSpec {
    fun configureRestAssuredSpecs() {
        RestAssured.config = RestAssuredConfig.config()
                .objectMapperConfig(
                        ObjectMapperConfig().jackson2ObjectMapperFactory { _, _ ->
                            ObjectMapper().registerModule(KotlinModule())
                                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        })
        RestAssured.requestSpecification = RequestSpecBuilder()
                .addHeader("Accept", "application/vnd.allegro.public.v1+json")
                .build()
    }

}

inline fun <reified T> ResponseBodyExtractionOptions.to(): T {
    return this.`as`(T::class.java)
}