package allegro.category

data class SaleCategories(
        var categories: List<SaleCategory>) {
    fun size(): Int = categories.size
}

data class SaleCategory(
        var id: String,
        var name: String,
        var parent: Parent?,
        var leaf: Boolean,
        var options: CategoryOptions

) {
    data class CategoryOptions(
            var variantsByColorPatternAllowed: Boolean,
            var advertisement: Boolean,
            var advertisementPriceOptional: Boolean,
            var offersWithProductPublicationEnabled: Boolean,
            var productCreationEnabled: Boolean,
            var productEANRequired: Boolean,
            var customParametersEnabled: Boolean
    )

    data class Parent(var id: String)
}

data class AllegroAuthResponse(
        var access_token: String,
        var token_type: String,
        var expires_in: Long,
        var scope: String,
        var allegro_api: String,
        var jti: String,
)

data class SaleParameters(var parameters: List<SaleParameter>) {
    data class SaleParameter(
            var id: String,
            var name: String,
            var type: String,
            var required: Boolean,
            var requiredForProduct: Boolean,
            var unit: Any?,
            var options: ParamOptions,
            var dictionary: List<ParamDictionaryValues>?,
            var restrictions: ParamRestrictions
    )

    data class ParamRestrictions(var multipleChoices: Boolean)
    data class ParamDictionaryValues(
            val id: String,
            val value: String,
            val dependsOnValueIds: List<Any>
    )

    data class ParamOptions(
            var variantsAllowed: Boolean,
            var variantsEqual: Boolean,
            var ambiguousValueId: Any?,
            var dependsOnParameterId: Any?,
            var describesProduct: Boolean,
            var customValuesEnabled: Boolean,
    )
}