package allegro.category

import allegro.category.AuthModule.authToken
import io.restassured.RestAssured
import io.restassured.module.kotlin.extensions.Extract
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assumptions.assumeFalse
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle

@TestInstance(Lifecycle.PER_CLASS)
class CategoryTest {

    @BeforeAll
    fun setup() {
        RestAssured.baseURI = "https://api.allegro.pl/sale/categories"
        RestAssured.authentication = RestAssured.oauth2(authToken())
    }

    @Test
    fun `get for all categories should return list of sale categories`() {
        val categories = requestForAllCategories()
        assertTrue(categories.size() > 1)
    }

    @Test
    fun `get for parent category should return its offspring`() {
        val parentCategoryId = "121882"

        val parentCategory = requestForSaleCategory(parentCategoryId)
        assumeFalse(parentCategory.leaf)

        val offspring = requestForOffspringCategories(parentCategoryId)
        assertTrue(offspring.categories.isNotEmpty())
    }

    @Test
    fun `get for non parent category should return leaf category`() {
        val leafCategoryId = "258389"
        val category = requestForSaleCategory(leafCategoryId)

        assertTrue(category.leaf)
    }

    @Test
    fun `get for category with parameters should return list of parameters`() {
        val categoryId = "121882"
        val paramsForCategory = requestForSaleParameters(categoryId)

        assertTrue(paramsForCategory.parameters.isNotEmpty())
    }

    private fun requestForSaleParameters(categoryId: String):SaleParameters =
            When { get("/$categoryId/parameters") }
            .Extract { response().body.to() }

    private fun requestForAllCategories(): SaleCategories {
        return When { get("") }
                .Then { statusCode(`is`(200)) }
                .Extract { response().body.to() }
    }

    private fun requestForOffspringCategories(parentCategoryId: String): SaleCategories {
        return Given { formParam("parent.id", parentCategoryId) }
                .When { get("") }
                .Then { statusCode(`is`(200)).log() }
                .Extract { response().body.to() }
    }

    private fun requestForSaleCategory(parentCategoryId: String): SaleCategory {
        return When { get("/$parentCategoryId") }
                .Then { statusCode(`is`(200)) }
                .Extract { response().body.to() }
    }
}
