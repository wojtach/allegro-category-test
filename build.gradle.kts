import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
    kotlin("jvm") version "1.4.10"
}

repositories {
    mavenCentral()
    jcenter()
    google()
}

kotlin {
    target
    sourceSets["main"].apply {
        kotlin.srcDir("src/main/kotlin")
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.4.10")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.10")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.4.10")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.4.10")
    implementation("io.rest-assured:rest-assured:4.3.1")
    implementation("io.rest-assured:kotlin-extensions:4.3.1")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.11.2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.11.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit5"))
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
    testImplementation("io.rest-assured:rest-assured:4.3.1")
    testImplementation("io.rest-assured:kotlin-extensions:4.3.1")
    testImplementation("com.fasterxml.jackson.core:jackson-databind:2.11.2")
    testImplementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.11.2")
}

tasks.test {
    systemProperties(Pair("allegroClientId", project.property("allegroClientId")), Pair("allegroClientSecret", project.property("allegroClientSecret")))
    useJUnitPlatform()
    testLogging{
        events(FAILED, SKIPPED, PASSED)
    }
}

